(function ($, Drupal) {
  // Minimum confidence, which is required by the model to verify the user.
  var maxDescriptorDistance = $('#tf_confidence').val();
  var modelDataPath = $('#model_data_path').val();
  var modelDataPathCustomFile = $('#model_data_path_custom_file').val();
  var infoContainer = $('#tf-info-container');
  var loadedLabeledFaceDescriptors = [];
  var faceMatcher;

  var image = $('<img id="tf-image" style="display:none;">');
  $('form.login-tensorflow-form').append(image);

  function addToLogMessage(message, params) {
    params = params || {};
    infoContainer.append($('<p>').html(Drupal.t(message, params)));
  }

  function loadModelFromFile() {
    // Get loaded data
    $.ajax({
      url: modelDataPathCustomFile,
      success: async function (data) {
        for (var element of data) {
          loadedLabeledFaceDescriptors.push(new faceapi.LabeledFaceDescriptors(
            element.label, [new Float32Array(element.descriptors[0])]));
        }
        faceMatcher = new faceapi.FaceMatcher(loadedLabeledFaceDescriptors, maxDescriptorDistance);
        addToLogMessage('Model data is loaded!')
      },
      error: function (jqXHR, textStatus, errorThrown) {
        addToLogMessage('The model is empty, please train the model!');
      },
      cache: false
    });
  }

  function startPredict(e) {
    e.preventDefault();
    var imagePredicted = $('#tf_capture')[0].files[0];
    var fileReader = new FileReader();
    fileReader.readAsDataURL(imagePredicted);
    fileReader.onloadend = async function (event) {
      var imageDom = document.getElementById("tf-image");
      imageDom.src = event.target.result;

      var fullFaceDescription = await faceapi.detectSingleFace(imageDom)
        .withFaceLandmarks().withFaceDescriptor();
      if (!fullFaceDescription || !faceMatcher) {
        addToLogMessage('No face recognized!');
        return;
      }
      const results = faceMatcher.findBestMatch(fullFaceDescription.descriptor);
      if (results.label === 'unknown') {
        addToLogMessage('Cannot recognize user! Try again');
        return;
      }

      addToLogMessage('Logging in...');
      $.post(window.location.origin + '/tensorflow/login', {
        'username': results.label,
        'tf_token': $('#tf_token').val()
      }).done(function (data) {
        if (data.error) {
          window.location.reload();
        }
        else {
          window.location.href = '/user/login';
        }
      });
    }
  }

  setTimeout(async function () {
    await faceapi.loadSsdMobilenetv1Model(modelDataPath);
    await faceapi.loadFaceRecognitionModel(modelDataPath);
    await faceapi.loadFaceLandmarkModel(modelDataPath);

    loadModelFromFile();
    $(document).on('click', '#tf_capture_submit', startPredict)
  }, 100)
})(jQuery, Drupal);
