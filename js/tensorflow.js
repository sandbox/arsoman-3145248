(function ($, Drupal) {
  var modelDataPath = $('#model_data_path').val();
  var modelDataPathCustomFile = $('#model_data_path_custom_file').val();
  var maxDescriptorDistance = $('#tf_confidence').val();
  var labeledFaceDescriptors = [];
  var infoContainer = $('#tf-info-container');
  var tfManageToken = $('#tf_manage_token').val();
  var loadedLabeledFaceDescriptors = [];
  var faceMatcher;

  function addToLogMessage(message, params) {
    params = params || {};
    infoContainer.append($('<p>').html(Drupal.t(message, params)));
  }

  function hideLoading() {
    $('.tf-action-area .tf-loader').removeClass('loader-visible');
  }

  function showLoading() {
    $('.tf-action-area .tf-loader').addClass('loader-visible');
  }

  function loadModelFromFile() {
    // Get loaded data
    if (!modelDataPathCustomFile) {
      addToLogMessage('The model is empty, please train the model!');
      return;
    }
    $.ajax({
      url: modelDataPathCustomFile,
      success: async function (data) {
        for (var element of data) {
          loadedLabeledFaceDescriptors.push(new faceapi.LabeledFaceDescriptors(
            element.label, [new Float32Array(element.descriptors[0])]));
        }
        faceMatcher = new faceapi.FaceMatcher(loadedLabeledFaceDescriptors, maxDescriptorDistance);
        addToLogMessage('Model data is loaded!')
      },
      error: function (jqXHR, textStatus, errorThrown) {
        addToLogMessage('The model is empty, please train the model!');
      },
      cache: false
    });
  }

  function startPredict() {
    var imagePredicted = $('#tf-image-for-predict')[0].files[0];
    var fileReader = new FileReader();
    fileReader.readAsDataURL(imagePredicted);
    fileReader.onloadend = async function (event) {
      var imageDom = document.getElementById("tf-image");
      imageDom.src = event.target.result;

      var fullFaceDescription = await faceapi.detectSingleFace(imageDom)
        .withFaceLandmarks().withFaceDescriptor();
      if (!fullFaceDescription || !faceMatcher) {
        addToLogMessage('No face recognized!');
        return;
      }
      const results = faceMatcher.findBestMatch(fullFaceDescription.descriptor);
      addToLogMessage('Confidence: ' + results.label + '<br/>' + (100 - results.distance.toFixed(2) * 100) + '%');
      console.log(results);
    }
  }

  function uploadTrainedData() {
    var fileData = JSON.stringify(labeledFaceDescriptors);
    $.ajax('/tensorflow/config/upload', {
      method: "POST",
      data: {fileData: fileData, tf_manage_token: tfManageToken},
      success: function (data) {
        console.log(data);
        addToLogMessage('Model data was successfully uploaded!');
      },
      error: function (err) {
        console.log(err)
        addToLogMessage('Something is wrong, try again later!');
      }
    });
  }

  function trainImages(images) {
    return new Promise(async function (resolve, reject) {
      for (var image of images) {
        var fileReader = new FileReader();
        fileReader.readAsDataURL(image);
        await new Promise(function (resolve, reject) {
          fileReader.onloadend = async function (event) {
            var imageDom = document.getElementById("tf-image");
            imageDom.src = event.target.result;
            var label = $('#tensorflow_user_selector option:selected').text();
            if (!label) {
              // User upload files for him.
              label = $('#tf_username').val();
            }
            if (!label) {
              console.error('Label is missing!!!');
              return;
            }
            // Add each image for training.
            const fullFaceDescription = await faceapi.detectSingleFace(imageDom).withFaceLandmarks().withFaceDescriptor()
            if (!fullFaceDescription) {
              throw new Error(`no faces detected for ${label}`);
            }
            const faceDescriptors = [fullFaceDescription.descriptor]
            labeledFaceDescriptors.push(new faceapi.LabeledFaceDescriptors(label, faceDescriptors));
            resolve();
          }
        })
      }
      resolve(labeledFaceDescriptors);
    });
  }

  async function addImages() {
    var images = $('#tf-images-for-training')[0].files;
    if (images.length > 0) {
      showLoading();
      Promise.all([
        trainImages(images)
      ])
        .then(([res]) => {
          addToLogMessage('Images was added and the model was trained! Please upload the model data!')
          hideLoading();
        })
        .catch(err => console.error(err));
    }
  }

  setTimeout(async function () {
    await faceapi.loadSsdMobilenetv1Model(modelDataPath);
    await faceapi.loadFaceRecognitionModel(modelDataPath);
    await faceapi.loadFaceLandmarkModel(modelDataPath);

    loadModelFromFile();
    $(document).on('change', '#tf-images-for-training', addImages);
    $(document).on('click', '#tf-images-for-training-submit', uploadTrainedData);
    $(document).on('click', '#tf-images-for-predict-submit', startPredict)
  }, 100)
})(jQuery, Drupal);
