(function ($, Drupal) {
  function toggleAllUsers() {
    var selectAllCheckbox = $(this);
    var isChecked = selectAllCheckbox.is(':checked');
    $('#edit-tf-user-list input[type="checkbox"]').prop('checked', isChecked);
  }

  function toggleAllRoles() {
    var selectAllCheckbox = $(this);
    var isChecked = selectAllCheckbox.is(':checked');
    $('#edit-tf-role-list input[type="checkbox"]').prop('checked', isChecked);
  }

  $(document)
    .off('change', '#edit-tf-select-all-users')
    .on('change', '#edit-tf-select-all-users', toggleAllUsers);

  $(document)
    .off('change', '#edit-tf-select-all-user-roles')
    .on('change', '#edit-tf-select-all-user-roles', toggleAllRoles);
})(jQuery, Drupal)
