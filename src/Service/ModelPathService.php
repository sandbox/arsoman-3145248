<?php

namespace Drupal\login_tensorflow\Service;

use Drupal\Core\File\FileSystem;

class ModelPathService {

  const FILE_NAME_JSON = 'tensorflow-login.json';

  /**
   * @var \Drupal\Core\File\FileSystem
   */
  private $fileSystem;

  /**
   * ModelPathService constructor.
   *
   * @param \Drupal\Core\File\FileSystem $fileSystem
   */
  public function __construct(FileSystem $fileSystem) {
    $this->fileSystem = $fileSystem;
  }

  public function getModelDataPath() {
    return $this->fileSystem->realpath("public://") . '/tf-files/models';
  }

  public function getModelDataURL() {
    return '/sites/default/files/tf-files/models';
  }

  public function getModelDataURLCustomFile() {
    return is_file($this->getModelCustomDataPathFile())
      ? '/sites/default/files/tf-files/' . self::FILE_NAME_JSON
      : NULL;
  }

  public function getModelCustomDataPathFile() {
    return $this->fileSystem->realpath("public://") . '/tf-files/' . self::FILE_NAME_JSON;
  }
}
