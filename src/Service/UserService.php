<?php

namespace Drupal\login_tensorflow\Service;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\login_tensorflow\Form\ConfigurationForm;
use Drupal\user\Entity\User;

class UserService {

  /**
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  private $configFactory;

  /**
   * @var \Drupal\Core\Config\Config
   */
  private $configStore;

  /**
   * @var \Drupal\Core\Session\AccountInterface
   */
  private $account;

  /**
   * UserService constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   * @param \Drupal\Core\Session\AccountInterface $account
   */
  public function __construct(ConfigFactoryInterface $configFactory, AccountInterface $account) {
    $this->configFactory = $configFactory;
    $this->configStore = $configFactory->getEditable(ConfigurationForm::EDITABLE_CONFIG_NAME);
    $this->account = $account;
  }

  public function getAllowedUsersForLoginAsDropdown() {
    $users = $this->getUsers();
    $allowedUserIds = $this->getAllowedUserIdsFromConfig();
    $usersAsDropdown = [];
    foreach ($users as $user) {
      if (in_array($user->id(), $allowedUserIds)) {
        $usersAsDropdown[$user->id()] = $user->getUsername();
      }
    }

    return $usersAsDropdown;
  }

  /**
   * @param bool $returnIds
   *
   * @return User[]|int
   */
  public function getUsers($returnIds = FALSE) {
    $userIds = \Drupal::entityQuery('user')
      ->condition('name', '', '!=')
      ->condition('status', 1)
      ->execute();
    if ($returnIds === TRUE) {
      return $userIds;
    }

    return User::loadMultiple($userIds);
  }

  private function getAllowedUserIdsFromConfig() {
    return $this->configStore->get('tf_allowed_users') ? array_filter($this->configStore->get('tf_allowed_users')) : [];
  }

  public function checkUserAccess(User $user) {
    return in_array($user->id(), $this->getAllowedUserIdsFromConfig());
  }

  public function checkIsUserAdmin() {
    return in_array('administrator', $this->account->getRoles());
  }

  public function generateToken() {
    return bin2hex(random_bytes(random_int(10, 30)));
  }
}
