<?php

namespace Drupal\login_tensorflow\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\login_tensorflow\Service\ModelPathService;
use Drupal\login_tensorflow\Service\UserService;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoginForm extends FormBase {

  /**
   * @var \Drupal\login_tensorflow\Service\ModelPathService
   */
  private $modelPathService;

  /**
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  private $privateTempStoreFactory;

  /**
   * @var \Drupal\Core\Config\Config
   */
  private $configStore;

  /**
   * @var \Drupal\login_tensorflow\Service\UserService
   */
  private $userService;

  /**
   * LoginForm constructor.
   *
   * @param \Drupal\login_tensorflow\Service\ModelPathService $modelPathService
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $privateTempStoreFactory
   * @param \Drupal\login_tensorflow\Service\UserService $userService
   */
  public function __construct(
    ModelPathService $modelPathService,
    PrivateTempStoreFactory $privateTempStoreFactory,
    UserService $userService
  ) {
    $this->modelPathService = $modelPathService;
    $this->privateTempStoreFactory = $privateTempStoreFactory->get('login_tensorflow');
    $this->configStore = $this->configFactory()
      ->getEditable(ConfigurationForm::EDITABLE_CONFIG_NAME);
    $this->userService = $userService;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('tf.model.service'),
      $container->get('tempstore.private'),
      $container->get('tf.user.service')
    );
  }

  public function getFormId() {
    return 'login_tensorflow_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    // Force to clear page cache, needed to be sure,
    // that a new token will be created on every page loading.
    \Drupal::service('page_cache_kill_switch')->trigger();
    $userIds = \Drupal::entityQuery('user')
      ->condition('name', '', '!=')
      ->execute();
    $countUsers = count($userIds);
    $users = User::loadMultiple($userIds);
    $tokens = [];
    $unigueKey = 'tf_token_' . $this->userService->generateToken();
    $tokens[] = $unigueKey;
    foreach ($users as $user) {
      /* @var $user \Drupal\user\Entity\User */
      $tokens[] = $this->userService->generateToken();
    }
    $tokens = implode(':', $tokens);
    $this->privateTempStoreFactory->set($unigueKey, $tokens);
    $modelDataPath = $this->modelPathService->getModelDataURL();
    $modelDataPathCustomFile = $this->modelPathService->getModelDataURLCustomFile();
    if ($modelDataPathCustomFile === NULL) {
      $this->messenger()
        ->addError($this->t('Cannot use this feature right now, please contact site administrator!'));
      return $form;
    }

    $form['countUsers'] = [
      '#type' => 'hidden',
      '#value' => $countUsers,
      '#attributes' => [
        'id' => 'count_users',
      ],
    ];
    $form['modelDataPath'] = [
      '#type' => 'hidden',
      '#value' => $modelDataPath,
      '#attributes' => [
        'id' => 'model_data_path',
      ],
    ];
    $form['modelDataPathCustomFile'] = [
      '#type' => 'hidden',
      '#value' => $modelDataPathCustomFile,
      '#attributes' => [
        'id' => 'model_data_path_custom_file',
      ],
    ];
    $form['tf_token'] = [
      '#type' => 'hidden',
      '#value' => $tokens,
      '#attributes' => [
        'id' => 'tf_token',
      ],
    ];
    $form['tf-info-container'] = [
      '#markup' => '<div id="tf-info-container"></div>',
    ];
    $form['photo'] = [
      '#type' => 'file',
      '#attributes' => [
        'id' => 'tf_capture',
        'name' => 'image',
        'accept' => 'image/png,image/jpeg,image/bmp',
        'capture' => 'user',
      ],
    ];
    $form['submit'] = [
      '#type' => 'button',
      '#value' => t('Login'),
      '#attributes' => [
        'id' => 'tf_capture_submit',
      ],
    ];
    $form['tf_confidence'] = [
      '#type' => 'hidden',
      '#value' => (100 - $this->configStore->get('tf_confidence')) / 100,
      '#attributes' => [
        'id' => 'tf_confidence',
      ],
    ];
    $form['#attached']['library'][] = 'login_tensorflow/tensorflow';
    $form['#attached']['library'][] = 'login_tensorflow/login';
    $form['#cache'] = ['max-age' => 0];

    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    // TODO: Implement submitForm() method.
  }
}
