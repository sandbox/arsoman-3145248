<?php


namespace Drupal\login_tensorflow\Form;


use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\login_tensorflow\Service\ModelPathService;
use Drupal\login_tensorflow\Service\UserService;
use Drupal\user\Entity\Role;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ConfigurationForm extends ConfigFormBase {

  const EDITABLE_CONFIG_NAME = 'login_tensorflow.config_form';

  /**
   * @var \Drupal\login_tensorflow\Service\ModelPathService
   */
  private $modelPathService;

  /**
   * @var \Drupal\Core\Config\Config
   */
  private $configStore;

  /**
   * @var \Drupal\login_tensorflow\Service\UserService
   */
  private $userService;

  /**
   * ConfigurationForm constructor.
   *
   * @param \Drupal\login_tensorflow\Service\ModelPathService $modelPathService
   * @param \Drupal\login_tensorflow\Service\UserService $userService
   */
  public function __construct(
    ModelPathService $modelPathService,
    UserService $userService
  ) {
    $this->modelPathService = $modelPathService;
    $this->configStore = $this->configFactory()
      ->getEditable(self::EDITABLE_CONFIG_NAME);
    $this->userService = $userService;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('tf.model.service'),
      $container->get('tf.user.service')
    );
  }

  public function getFormId() {
    return 'login_tensorflow_config_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $users = $this->userService->getUsers();
    $optionsUserList = [];
    foreach ($users as $user) {
      $optionsUserList[$user->id()] = $user->getUsername();
    }
    $optionsRoleList = [];
    $roles = Role::loadMultiple();
    unset($roles['anonymous']);
    //    unset($roles['authenticated']);
    foreach ($roles as $role) {
      /* @var $role Role */
      $label = $role->label();
      if ($role->id() == 'authenticated') {
        $label .= $this->t(' %text', ['%text' => '(This will allow all type of users, no matter of other selections!)']);
      }
      $optionsRoleList[$role->id()] = $label;
    }
    $form['tf_confidence'] = [
      '#type' => 'number',
      '#title' => 'Confidence',
      '#default_value' => $this->configStore->get('tf_confidence'),
      '#min' => 50,
      '#max' => 100,
      '#description' => 'Set the desired accuracy for image recognition',
    ];
    $form['tf_allowed_users'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => $this->t('Select users or user roles'),
      '#description' => $this->t('Select which users or type of users will be allowed to login via image capture'),
      'tf_user_choise' => [
        '#type' => 'radios',
        '#default_value' => $this->configStore->get('tf_user_choise') ?? 0,
        '#options' => [
          $this->t('Choose users'),
          $this->t('Choose roles'),
        ],
      ],
      'tf_user_list' => [
        '#type' => 'fieldset',
        '#title' => $this->t('Select users by username'),
        '#description' => $this->t('Only selected users will have possibility to login via image capture!'),
        '#states' => [
          'visible' => [
            'input[name="tf_user_choise"]' => ['value' => 0],
          ],
        ],
        'tf_select_all_users' => [
          '#type' => 'checkbox',
          '#title' => $this->t('Select/Deselect all'),
          '#default_value' => count($this->userService->getAllowedUsersForLoginAsDropdown()) === count($optionsUserList),
        ],
        'tf_allowed_users' => [
          '#type' => 'checkboxes',
          '#options' => $optionsUserList,
          '#default_value' => $this->configStore->get('tf_allowed_users'),
        ],
      ],
      'tf_role_list' => [
        '#type' => 'fieldset',
        '#title' => $this->t('Select users by roles'),
        '#description' => $this->t('Only selected user roles will have possibility to login via image capture!'),
        '#states' => [
          'visible' => [
            'input[name="tf_user_choise"]' => ['value' => 1],
          ],
        ],
        'tf_select_all_user_roles' => [
          '#type' => 'checkbox',
          '#title' => $this->t('Select/Deselect all'),
          '#default_value' => count($this->configStore->get('tf_allowed_roles')) === count($optionsRoleList),
        ],
        'tf_allowed_roles' => [
          '#type' => 'checkboxes',
          '#options' => $optionsRoleList,
          '#default_value' => $this->configStore->get('tf_allowed_roles'),
        ],
      ],
    ];
    $form['tf_delete_data'] = [
      '#type' => 'details',
      '#title' => $this->t('Delete all model data'),
      '#description' => $this->t('This will delete all model data, no users will be recognized until new training is performed!'),
      'tf_delete_data_submit' => [
        '#type' => 'link',
        '#title' => $this->t('Delete all model data!'),
        '#url' => Url::fromRoute('login_tensorflow.delete_model_data'),
      ],
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];
    $form['#attached']['library'][] = 'login_tensorflow/configuration';

    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $tfAllowedUsersIds = [];
    if ($form_state->getValue('tf_user_choise') == 0) {
      $tfAllowedUsersIds = $form_state->getValue('tf_allowed_users');
    }
    elseif ($form_state->getValue('tf_user_choise') == 1) {
      $selectedRoles = array_keys(Role::loadMultiple($form_state->getValue('tf_allowed_roles')));
      $this->configStore->set('tf_allowed_roles', $selectedRoles);
      if ($selectedRoles) {
        if (in_array('authenticated', $selectedRoles)) {
          $tfAllowedUsersIds = $this->userService->getUsers(TRUE);
        }
        else {
          $tfAllowedUsersIds = \Drupal::entityQuery('user')
            ->condition('status', 1)
            ->condition('roles', $selectedRoles, 'IN')
            ->execute();
        }
      }
    }
    $this->configStore->set('tf_confidence', $form_state->getValue('tf_confidence'));
    $this->configStore->set('tf_user_choise', $form_state->getValue('tf_user_choise'));
    $this->configStore->set('tf_allowed_users', $tfAllowedUsersIds);
    $this->configStore->save();
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {
    $confidence = $form_state->getValue('tf_confidence');
    if ($confidence < 50 || $confidence > 100) {
      $form_state->setErrorByName('tf_confidence', $this->t('Confidence must be between 50 and 100!'));
    }
  }

  protected function getEditableConfigNames() {
    return [self::EDITABLE_CONFIG_NAME];
  }
}
