<?php

namespace Drupal\login_tensorflow\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\Core\Url;
use Drupal\login_tensorflow\Form\ConfigurationForm;
use Drupal\login_tensorflow\Service\ModelPathService;
use Drupal\login_tensorflow\Service\UserService;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

class AdminController extends ControllerBase {

  const TF_MANAGE_TOKEN = 'tf_manage_token';

  /**
   * @var \Drupal\login_tensorflow\Service\ModelPathService
   */
  private $modelPathService;

  /**
   * @var \Drupal\Core\TempStore\PrivateTempStore
   */
  private $privateTempStore;

  /**
   * @var \Drupal\login_tensorflow\Service\UserService
   */
  private $userService;

  /**
   * @var \Drupal\Core\Config\Config
   */
  private $configStore;

  /**
   * AdminController constructor.
   *
   * @param \Drupal\login_tensorflow\Service\ModelPathService $modelPathService
   * @param \Drupal\login_tensorflow\Service\UserService $userService
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $privateTempStoreFactory
   */
  public function __construct(
    ModelPathService $modelPathService,
    UserService $userService,
    PrivateTempStoreFactory $privateTempStoreFactory
  ) {
    $this->modelPathService = $modelPathService;
    $this->privateTempStore = $privateTempStoreFactory->get('login_tensorflow');
    $this->userService = $userService;
    $this->configStore = \Drupal::configFactory()
      ->getEditable(ConfigurationForm::EDITABLE_CONFIG_NAME);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('tf.model.service'),
      $container->get('tf.user.service'),
      $container->get('tempstore.private')
    );
  }

  public function manageModel() {
    $users = $this->userService->getUsers();
    $countUsers = count($users);
    $modelDataPath = $this->modelPathService->getModelDataURL();
    $modelDataPathCustomFile = $this->modelPathService->getModelDataURLCustomFile();
    $tfConfidence = (100 - $this->configStore->get('tf_confidence')) / 100;
    $tfManageToken = self::TF_MANAGE_TOKEN . '_' . $this->userService->generateToken();
    $this->privateTempStore->set(self::TF_MANAGE_TOKEN, $tfManageToken);

    return [
      '#theme' => 'manage_model',
      '#userSelector' => [
        '#type' => 'select',
        '#options' => $this->userService->getAllowedUsersForLoginAsDropdown(),
        '#attributes' => [
          'id' => ['tensorflow_user_selector'],
        ],
      ],
      '#modelDataPath' => $modelDataPath,
      '#modelDataPathCustomFile' => $modelDataPathCustomFile,
      '#countUsers' => $countUsers,
      '#tfConfidence' => $tfConfidence,
      '#tfManageToken' => $tfManageToken,
    ];
  }

  public function handleUploadModel() {
    $response = ['success' => FALSE];
    $req = \Drupal::request();
    if ($token = $req->get(self::TF_MANAGE_TOKEN)) {
      if ($token !== $this->privateTempStore->get(self::TF_MANAGE_TOKEN)) {
        return $response;
      }
      try {
        $fileData = json_decode($req->get('fileData'), TRUE);
        $filename = $this->modelPathService->getModelCustomDataPathFile();
        $oldData = json_decode(file_get_contents($filename), TRUE);
        if (!$oldData) {
          $fh = fopen($filename, 'w');
          fwrite($fh, json_encode($fileData));
          fclose($fh);
          $response = ['success' => TRUE];
        }
        else {
          $fileData = array_merge($oldData, $fileData);
          file_put_contents($filename, json_encode($fileData));
          $response = ['success' => TRUE];
        }
      } catch (\Exception $exception) {
        $response = ['error' => $exception->getMessage()];
      }
    }

    return new JsonResponse($response);
  }

  /**
   * Provide functionality to unattended user login via tensorflow recognition.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   */
  public function login() {
    $response = [];
    $req = \Drupal::request();
    if ($token = $req->get('tf_token')) {
      $receivedTokenData = explode(':', $token);
      if (is_array($receivedTokenData)) {
        $sessionKey = $receivedTokenData[0];
        /* @var $savedTokenData \Drupal\Core\TempStore\PrivateTempStore */
        $savedTokenData = $this->privateTempStore->get($sessionKey);
        $this->privateTempStore->delete($sessionKey);
        if ($token === $savedTokenData) {
          $username = $req->get('username');
          $userEntity = user_load_by_name($username);
          if ($this->userService->checkUserAccess($userEntity)) {
            user_login_finalize(User::load($userEntity->id()));
          }
          else {
            $this->messenger()->addError($this->t('Not authorized'));
          }
        }
        else {
          $this->messenger()->addError($this->t('Invalid request data'));
          $response['error'] = 'Invalid request data';
        }
      }
    }
    return new JsonResponse($response);
  }

  public function deleteModel() {
    if (!$this->userService->checkIsUserAdmin()) {
      return new JsonResponse('Not authorized', 403);
    }
    $fileName = $this->modelPathService->getModelCustomDataPathFile();
    if (is_file($fileName)) {
      unlink($fileName);
    }
    $url = Url::fromRoute('login_tensorflow.manage_model')->toString();
    $this->messenger()
      ->addMessage($this->t('Successfully deleted all data! You need to train the model again. <a href=":link">train</a>',
        [':link' => $url]));
    return $this->redirect('login_tensorflow.configuration');
  }

  public function allowAccessAnonymous(AccountInterface $account) {
    return $account->isAnonymous() ? AccessResult::allowed() : AccessResult::forbidden();
  }
}
